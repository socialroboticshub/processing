""" All Credits goes to https://github.com/vjgpt/Face-and-Emotion-Recognition """
import cv2
import numpy as np
from dlib import get_frontal_face_detector
from imutils import face_utils
from sic.service import SICservice
from tensorflow.python.keras.models import \
    load_model  # direct import from keras has a bug see: https://stackoverflow.com/a/59810484/3668659
from threading import Event, Thread

from utils.datasets import get_labels
from utils.inference import apply_offsets
from utils.preprocessor import preprocess_input


class EmotionDetectionService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        self.is_detecting = False
        self.img_timestamp = None
        self.img_available = Event()

        # Emotion detection parameters
        self.emotion_labels = get_labels('fer2013')
        # hyper-parameters for bounding boxes shape
        self.frame_window = 10
        self.emotion_offsets = (20, 40)
        # loading models
        self.detector = get_frontal_face_detector()
        self.emotion_classifier = load_model('emotion_model.hdf5', compile=False)
        # getting input model shapes for inference
        self.emotion_target_size = self.emotion_classifier.input_shape[1:3]

        super().__init__(connect, identifier, disconnect)

    def get_device_types(self):
        return ['cam']

    def get_channel_action_mapping(self):
        return {self.get_full_channel('events'): self.execute,
                self.get_full_channel('image_available'): self.set_image_available}

    def execute(self, message):
        data = message['data'].decode()
        if data == 'WatchingStarted':
            if not self.is_detecting:
                self.is_detecting = True
                self.update_image_properties()
                emotion_detection_thread = Thread(target=self.detect_emotion)
                emotion_detection_thread.start()
            else:
                print('Emotion detection already running for ' + self.identifier)
        elif data == 'WatchingDone':
            if self.is_detecting:
                self.is_detecting = False
                self.img_available.set()
            else:
                print('Emotion detection already stopped for ' + self.identifier)

    def detect_emotion(self):
        self.produce_event('EmotionDetectionStarted')
        while self.is_detecting:
            if self.img_available.is_set():
                timestamp = self.img_timestamp
                if timestamp is None:
                    continue
                image = self.retrieve_image(timestamp)
                self.img_available.clear()
                if image is None:
                    continue

                gray_image = self.img_to_opencv(image, want_color=False)
                rgb_image = self.img_to_opencv(image, want_color=True)

                # Detect all faces in the image and run the classifier on them
                faces = self.detector(rgb_image)
                for face_coordinates in faces:
                    x1, x2, y1, y2 = apply_offsets(face_utils.rect_to_bb(face_coordinates), self.emotion_offsets)
                    gray_face = gray_image[y1:y2, x1:x2]
                    gray_face = cv2.resize(gray_face, self.emotion_target_size)
                    gray_face = preprocess_input(gray_face, True)
                    gray_face = np.expand_dims(gray_face, 0)
                    gray_face = np.expand_dims(gray_face, -1)
                    emotion_prediction = self.emotion_classifier.predict(gray_face)

                    # Get the emotion predicted as most probable
                    emotion_label_arg = np.argmax(emotion_prediction)
                    emotion_text = self.emotion_labels[emotion_label_arg]
                    print(self.identifier + ': detected ' + emotion_text)
                    self.publish('detected_emotion', emotion_text)
            else:
                self.img_available.wait()
        self.produce_event('EmotionDetectionDone')

    def set_image_available(self, message):
        raw_timestamp = message['data'].decode()
        self.img_timestamp = int(raw_timestamp)
        self.img_available.set()

    def cleanup(self):
        self.is_detecting = False
        self.img_available.set()
