from sic.factory import SICfactory

from emotion_detection_service import EmotionDetectionService


class EmotionDetectionFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'emotion_detection'

    def create_service(self, connect, identifier, disconnect):
        return EmotionDetectionService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = EmotionDetectionFactory()
    factory.run()
