from json import loads

from google.cloud import texttospeech
from google.oauth2 import service_account
from sic.service import SICservice


class TTSService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        super(TTSService, self).__init__(connect, identifier, disconnect)
        self.lang = None
        self.voice = None
        self.client = None
        self.audio_config = texttospeech.AudioConfig(audio_encoding=texttospeech.AudioEncoding.LINEAR16)

    def get_device_types(self):
        return ['speaker']

    def get_channel_action_mapping(self):
        return {
            self.get_full_channel('tts_voice'): self.set_voice,
            self.get_full_channel('tts_key'): self.set_key,
            self.get_full_channel('text_to_speech'): self.synthesize_text
        }

    def set_voice(self, message: str) -> None:
        """set_voice method
        :param str message: the voice code of the Google TTS Agent

        Set the language and the voice of the Google TTS Agent
        """
        tts_voice = message['data'].decode()
        self.lang = tts_voice[:5]
        self.voice = texttospeech.VoiceSelectionParams(language_code=self.lang, name=tts_voice)

    def set_key(self, message: str) -> None:
        """set_key method
        :param str message: Google Agent Key file

        Authenticates TTS Google Agent with given key"""
        key = message['data'].decode()

        credentials = service_account.Credentials.from_service_account_info(loads(key))
        self.client = texttospeech.TextToSpeechClient(credentials=credentials)

    def synthesize_text(self, message: str) -> None:
        """Synthesizes speech from the input string of text.
        The response received is a JSON datastructure with only the audio content,
        as per https://cloud.google.com/text-to-speech/docs/create-audio-text-command-line."""
        text = message['data'].decode()

        input_text = texttospeech.SynthesisInput(text=text)

        response = self.client.synthesize_speech(
            request={'input': input_text, 'voice': self.voice, 'audio_config': self.audio_config}
        )

        self.publish('action_play_audio', response.audio_content)
