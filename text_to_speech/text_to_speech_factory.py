from sic.factory import SICfactory

from text_to_speech_service import TTSService


class TTSFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'text_to_speech'

    def create_service(self, connect, identifier, disconnect):
        return TTSService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = TTSFactory()
    factory.run()
