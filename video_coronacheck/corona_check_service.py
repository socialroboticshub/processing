from os import environ
from threading import Event, Thread

from coronacheck_tools.clitools import convert
from coronacheck_tools.verification.verifier import validate_raw
from sic.service import SICservice


class CoronaCheckService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        self.is_checking = False
        self.img_timestamp = None
        self.img_available = Event()

        # QR code options
        self.allow_international = False
        environ['XDG_CONFIG_HOME'] = '/coronacheck/.config'

        super().__init__(connect, identifier, disconnect)

    def get_device_types(self):
        return ['cam']

    def get_channel_action_mapping(self):
        return {self.get_full_channel('events'): self.execute,
                self.get_full_channel('image_available'): self.set_image_available}

    def execute(self, message):
        data = message['data'].decode()
        if data == 'WatchingStarted':
            if not self.is_checking:
                self.is_checking = True
                self.update_image_properties()
                corona_check_thread = Thread(target=self.corona_check)
                corona_check_thread.start()
            else:
                print('Corona checker already running for ' + self.identifier)
        elif data == 'WatchingDone':
            if self.is_checking:
                self.is_checking = False
                self.image_available_flag.set()
            else:
                print('Corona checker already stopped for ' + self.identifier)

    def corona_check(self):
        self.produce_event('CoronaCheckStarted')
        while self.is_checking:
            if self.img_available.is_set():
                img_timestamp = self.img_timestamp
                if img_timestamp is None:
                    continue
                image = self.retrieve_image(img_timestamp)
                self.img_available.clear()
                if image is None:
                    continue

                input_data = self.img_to_opencv(image)
                # imwrite('/coronacheck/' + str(time_ns()) + '.jpg', input_data)

                data = convert('QR', input_data, 'RAW')
                if isinstance(data, list):
                    data = data[0] if len(data) > 0 else None  # if we have multiple QR codes only verify the first one
                if data:
                    result = validate_raw(data, allow_international=self.allow_international)
                    if result[0]:
                        self.publish('corona_check', '1')
            else:
                self.img_available.wait()
        self.produce_event('CoronaCheckDone')

    def set_image_available(self, message):
        raw_timestamp = message['data'].decode()
        self.img_timestamp = int(raw_timestamp)
        self.img_available.set()

    def cleanup(self):
        self.is_checking = False
        self.img_available.set()
