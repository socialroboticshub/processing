package org.bitbucket.socialroboticshub;

import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.Protocol;

public abstract class SICfactory {
	private final Map<String, SICservice> active = new ConcurrentHashMap<>();

	public void run() throws Exception {
		try (final Jedis redis = connect()) {
			System.out.println("Subscribing the service factory...");
			redis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					if (SICfactory.this.active.containsKey(message)) {
						System.out.println("Reusing already running service for " + message);
					} else {
						System.out.println("Launching new service for " + message);
						try {
							final SICservice service = createService(message);
							service.start();
							SICfactory.this.active.put(message, service);
						} catch (final Exception e) {
							e.printStackTrace();
						}
					}
				}
			}, getConnectionChannel());
		}
	}

	protected abstract String getConnectionChannel();

	protected abstract SICservice createService(final String identifier) throws Exception;

	public void disconnected(final String identifier) {
		this.active.remove(identifier);
	}

	protected Jedis connect() throws Exception {
		Jedis jedis;
		final boolean selfsigned = "1".equals(System.getenv("DB_SSL_SELFSIGNED"));
		if (selfsigned) {
			final KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(SICfactory.class.getResourceAsStream("/truststore.jks"), "changeit".toCharArray());
			final Certificate original = ((KeyStore.TrustedCertificateEntry) keyStore.getEntry("sic", null))
					.getTrustedCertificate();
			final TrustManager bypass = new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[] { (X509Certificate) original };
				}

				@Override
				public void checkClientTrusted(final X509Certificate[] chain, final String authType)
						throws CertificateException {
					checkServerTrusted(chain, authType);
				}

				@Override
				public void checkServerTrusted(final X509Certificate[] chain, final String authType)
						throws CertificateException {
					if (chain.length != 1 || !chain[0].equals(original)) {
						throw new CertificateException("Invalid certificate provided");
					}
				}
			};
			final SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[] { bypass }, null);
			final SSLSocketFactory sslFactory = sslContext.getSocketFactory();
			jedis = new Jedis(System.getenv("DB_IP"), Protocol.DEFAULT_PORT, true, sslFactory, null, null);
		} else {
			jedis = new Jedis(System.getenv("DB_IP"), Protocol.DEFAULT_PORT, true);
		}
		jedis.auth(System.getenv("DB_PASS"));
		return jedis;
	}
}
