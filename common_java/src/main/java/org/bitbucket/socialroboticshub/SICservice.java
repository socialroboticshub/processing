package org.bitbucket.socialroboticshub;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Response;

public abstract class SICservice extends Thread {
	private static final Charset UTF8 = StandardCharsets.UTF_8;
	protected final SICfactory parent;
	protected final String identifier;
	protected final Jedis publisher, subscriber;
	protected final BinaryJedisPubSub pubsub;
	protected volatile boolean isRunning;

	SICservice(final SICfactory parent, final String identifier) throws Exception {
		this.parent = parent;
		this.identifier = identifier;
		this.publisher = parent.connect();
		this.subscriber = parent.connect();
		this.pubsub = new BinaryJedisPubSub() {
			private final int cutoff = (SICservice.this.identifier.length() + 1);

			@Override
			public void onMessage(final byte[] rawchannel, final byte[] data) {
				try {
					final String channel = bytesToString(rawchannel).substring(this.cutoff);
					SICservice.this.onMessage(channel, data);
				} catch (final Exception e) {
					e.printStackTrace();
					close();
				}
			}
		};
		checkIfAlive();
	}

	protected String getFullChannel(final String channelName) {
		return (this.identifier + "_" + channelName);
	}

	protected abstract DeviceType[] getDeviceTypes();

	protected abstract String[] getSubscriptions();

	protected abstract void onMessage(final String channel, final byte[] data) throws Exception;

	@Override
	public final void run() {
		final String[] topics = getSubscriptions();
		final List<byte[]> topicsWithId = new ArrayList<>(topics.length);
		for (final String topic : topics) {
			topicsWithId.add((this.identifier + "_" + topic).getBytes(UTF8));
		}
		System.out.println("Subscribing '" + this.identifier + "'");
		this.isRunning = true;
		this.subscriber.subscribe(this.pubsub, topicsWithId.toArray(new byte[topicsWithId.size()][]));
	}

	protected void produceEvent(final String event) {
		this.publisher.publish(getFullChannel("events"), event);
	}

	protected String getUserId() {
		return this.identifier.split("-")[0];
	}

	protected String getDeviceId() {
		return this.identifier.split("-")[1];
	}

	private void checkIfAlive() {
		new Thread(() -> {
			final String user = "user:" + getUserId();
			final String deviceId = getDeviceId();
			final DeviceType[] forTypes = getDeviceTypes();
			final List<String> devices = new ArrayList<>(forTypes.length);
			for (final DeviceType type : forTypes) {
				devices.add(deviceId + ":" + type);
			}
			while (this.isRunning) {
				try(final Pipeline pipe = new Pipeline(this.publisher)) {
					final Response<List<String>> timeFetch = pipe.time();
					final List<Response<Double>> scoreFetches = new ArrayList<>(devices.size());
					for (final String device : devices) {
						scoreFetches.add(pipe.zscore(user, device));
					}
					pipe.sync();

					final int time = Integer.parseInt(timeFetch.get().get(0));
					final List<Double> scores = new ArrayList<>(scoreFetches.size());
					for (final Response<Double> scoreFetch : scoreFetches) {
						scores.add(scoreFetch.get());
					}

					final long oneMinute = (time - 60);
					boolean foundOne = false;
					for (final Double rawScore : scores) {
						final int score = (rawScore == null) ? -1 : rawScore.intValue();
						if (score >= oneMinute) {
							foundOne = true;
							break;
						}
					}
					if (foundOne) {
						Thread.sleep(60 * 1000 + 100);
						continue;
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
				// shutdown
				if (this.isRunning) {
					System.out.println("Shutting down service for " + this.identifier + " (no longer alive)");
					close();
					break;
				}
			}
		}).start();
	}

	protected void close() {
		this.isRunning = false;
		this.pubsub.unsubscribe();
		this.subscriber.close();
		this.publisher.close();
		this.parent.disconnected(this.identifier);
	}

	protected static String bytesToString(final byte[] data) {
		return (data == null) ? null : new String(data, UTF8);
	}

	protected static byte[] stringToBytes(final String data) {
		return (data == null) ? null : data.getBytes(UTF8);
	}
}
