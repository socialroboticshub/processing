from sic.factory import SICfactory

from object_detection_service import ObjectDetectionService


class ObjectDetectionFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'object_detection'

    def create_service(self, connect, identifier, disconnect):
        return ObjectDetectionService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = ObjectDetectionFactory()
    factory.run()
