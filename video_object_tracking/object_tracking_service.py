from threading import Thread

from numpy import array, argsort, argpartition, float32, linalg, min, mean, median, ndarray, std, where, zeros_like
from sic.image_depth_pb2 import ImageDepth
from sic.image_masks_pb2 import ImageMasks
from sic.service import SICservice
from sic.tracking_result_pb2 import TrackingResult

MAX_OBJECTS_TRACKING = 3
CRITERION = 'median'


class ObjectTrackingService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        self.is_running = False
        self.img_timestamp = None
        self.segmentation_bytes = None

        self.tracking = None
        self.ids = None
        self.reset()

        super().__init__(connect, identifier, disconnect)
        self.events_topic = self.get_full_channel('events')
        self.segmentation_topic = self.get_full_channel('segmentation_stream')
        self.depth_topic = self.get_full_channel('depth_stream')
        self.tracked_object = self.get_full_channel('tracked_object')

    def reset(self):
        self.tracking = None
        self.ids = list(range(MAX_OBJECTS_TRACKING))[::-1]

    def get_device_types(self):
        return ['cam']

    def get_channel_action_mapping(self):
        return {self.get_full_channel('events'): self.execute}

    def execute(self, message):
        data = message['data'].decode()
        if data.startswith('DepthEstimationStarted'):
            split = data.split(';')
            self.img_timestamp = int(split[1])
            self.segmentation_bytes = self.redis.zrangebyscore(self.segmentation_topic, self.img_timestamp,
                                                               self.img_timestamp)
        elif data == 'DepthEstimationDone':
            depth_bytes = self.redis.zrangebyscore(self.depth_topic, self.img_timestamp, self.img_timestamp)
            if self.segmentation_bytes and depth_bytes:
                img_segmentation = ImageMasks()
                img_segmentation.ParseFromString(self.segmentation_bytes[0])
                depth_img = ImageDepth()
                depth_img.ParseFromString(depth_bytes[0])
                tracking_thread = Thread(target=self.track_objects,
                                         args=(self.img_timestamp, img_segmentation, depth_img))
                tracking_thread.start()
                print('Object tracking started for ' + self.identifier)
            else:
                print('Could not resolve timestamp to depth image: ' + str(self.img_timestamp))

    def track_objects(self, current_time, masks, depth_img):
        self.produce_event('ObjectTrackingStarted;' + str(current_time))

        prediction_masks = array(masks.masks).reshape((masks.mask_count, masks.mask_height, masks.mask_width))
        prediction_masks = prediction_masks.astype(bool)
        if len(prediction_masks) == 0:
            print('No objects in frame')
            self.reset()
            return

        depth_img = array(depth_img.depth_image).reshape((depth_img.image_height, depth_img.image_width))
        if depth_img is None:  # Only use segmentation masks for tracking
            depth_img = zeros_like(prediction_masks[0])

        self.track(current_time, prediction_masks, depth_img)

        results = []
        for tracked_object in self.tracking:
            print('Tracked object: ' + str(tracked_object))
            object_id = tracked_object[0]
            result = TrackingResult()
            result.timestamp_ms = current_time
            result.object_id = object_id
            result.distance_cm = self.get_dist_by_id(object_id)

            centroid = self.get_centroid_by_id(object_id)  # [y, x]
            result.centroid_x = centroid[1]
            result.centroid_y = centroid[0]

            result.in_frame_ms = self.get_in_frame_by_id(object_id)
            result.speed_cmps = self.get_speed_by_id(object_id)
            results.append(result)

        # Publish the tracking results
        pipe = self.redis.pipeline()
        pipe.publish(self.events_topic, 'ObjectTrackingDone')
        for result in results:
            pipe.publish(self.tracked_object, result.SerializeToString())
        pipe.execute()

    def track(self, current_time, masks, depth_img):
        if self.tracking is None:
            self.tracking = self.track_init(current_time, masks, depth_img)
        elif masks.shape[0] <= len(self.tracking):
            new_tracking = self.track_easy(current_time, masks, depth_img)
            id_s = self.get_ids(self.tracking) - self.get_ids(new_tracking)
            self.ids.extend(id_s)
            self.tracking = new_tracking
        else:
            self.tracking = self.track_hard(current_time, masks, depth_img)

    def track_init(self, current_time, masks, depth_img):
        new_tracking = []
        for mask in masks:
            dist = self.get_distance(mask, depth_img)
            centroid = self.get_centroid(mask)
            result = self.track_new_persons(current_time, dist, centroid)
            if result:
                new_tracking.append(result)
            else:
                break

        return self.save_return(new_tracking)

    def get_closest_match(self, candidate, used_ids):
        temp_dist = candidate - self.tracking[:, 1:4]
        relative_dist = linalg.norm(temp_dist.astype(float32), axis=1)
        possible_idx = argsort(relative_dist)

        for i, min_dist_id in enumerate(possible_idx):
            tracking_id = self.tracking[min_dist_id][0]
            if tracking_id in used_ids:
                return tracking_id

        return None

    @staticmethod
    def get_ids(tracks):
        if tracks is None:
            return set()
        return set(tracks[:, 0])

    @staticmethod
    def save_return(tracking):
        # No data
        if not any(tracking):
            return None

        # Is already in right format
        if isinstance(tracking, ndarray) and len(tracking.shape) == 2:
            return tracking

        # Conversion required
        return array(tracking, dtype='object')

    @staticmethod
    def get_centroid(mask):
        return array(where(mask == 1)).mean(axis=1).astype(int)  # [y, x]

    @staticmethod
    def get_distance(mask, depth_img):
        return int(ObjectTrackingService.img_to_depth(depth_img[mask])[0])

    @staticmethod
    def img_to_depth(img):
        if CRITERION == 'mean':
            return int(mean(img)), int(std(img))
        elif CRITERION == 'median':
            return int(median(img)), int(std(img))
        else:
            raise ValueError('Criterion ' + CRITERION + ' not known; should be "mean" or "median"')

    def get_entry_by_id(self, tracking_id):
        if self.tracking is None:
            return None
        else:
            return self.tracking[self.tracking[:, 0] == tracking_id]

    def get_dist_by_id(self, tracking_id):
        return self.get_entry_by_id(tracking_id)[0, 1]

    def get_centroid_by_id(self, tracking_id):
        return self.get_entry_by_id(tracking_id)[0, 2:4]

    def get_in_frame_by_id(self, tracking_id):
        return self.get_entry_by_id(tracking_id)[0, 4]

    def get_time_stamp_by_id(self, tracking_id):
        return self.get_entry_by_id(tracking_id)[0, 5]

    def get_speed_by_id(self, tracking_id):
        return self.get_entry_by_id(tracking_id)[0, 6]

    def track_new_persons(self, current_time, dist, centroid):
        if self.ids:
            tracking_id = self.ids.pop()
            return [tracking_id, dist, centroid[0], centroid[1], 0, current_time, 0]
        else:
            return None

    def track_old_persons(self, current_time, dist, centroid, ids_in_use):
        feature_vec = array([dist, centroid[0], centroid[1]])

        # Find closest match for current person
        tracking_id = self.get_closest_match(feature_vec, ids_in_use)
        ids_in_use.remove(tracking_id)

        delta_t = current_time - self.get_time_stamp_by_id(tracking_id)
        in_frame = int(self.get_in_frame_by_id(tracking_id) + delta_t)  # Time (ms) in frame
        speed = int((dist - self.get_dist_by_id(tracking_id)) / (delta_t / 1000 + 1e-10))  # Speed (cm/s)

        return [tracking_id, dist, centroid[0], centroid[1], in_frame, current_time, speed]

    def track_easy(self, current_time, masks, depth_img):
        new_tracking, ids_in_use = [], self.get_ids(self.tracking)
        for mask in masks:
            dist = self.get_distance(mask, depth_img)
            centroid = self.get_centroid(mask)
            result = self.track_old_persons(current_time, dist, centroid, ids_in_use)
            new_tracking.append(result)

        return self.save_return(new_tracking)

    def track_hard(self, current_time, masks, depth_img):
        temp = []
        for mask in masks:
            dist = self.get_distance(mask, depth_img)
            centroid = self.get_centroid(mask)

            temp_dist = array([dist, centroid[0], centroid[1]]) - self.tracking[:, 1:4]
            dist_c = min(linalg.norm(temp_dist.astype(float32), axis=1))
            temp.append([dist_c, dist, centroid, mask, None])

        temp = array(temp, dtype='object')
        n_new_persons = masks.shape[0] - len(self.tracking)
        partition = argpartition(temp[:, 0], -n_new_persons)

        new_persons = partition[-n_new_persons:]
        old_persons = partition[:-n_new_persons]

        new_tracking_out, ids_in_use = [], set(self.tracking[:, 0])
        for old_person in old_persons:
            _, dist, centroid, mask, _ = temp[old_person]
            result = self.track_old_persons(current_time, dist, centroid, ids_in_use)
            new_tracking_out.append(result)

        for new_person in new_persons:
            _, dist, centroid, mask, _ = temp[new_person]
            result = self.track_new_persons(current_time, dist, centroid)
            if result:
                new_tracking_out.append(result)
            else:
                break

        return self.save_return(new_tracking_out)

    def cleanup(self):
        self.is_running = False
