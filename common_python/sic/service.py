from threading import Thread
from time import sleep

STEREO_RATIO = (1280 / 360)


class SICservice(object):
    def __init__(self, connect, identifier, disconnect):
        self.redis = connect()
        self.identifier = identifier
        self.disconnect = disconnect
        self.running = True

        # Redis initialization
        print('Subscribing ' + identifier)
        self.pubsub = self.redis.pubsub(ignore_subscribe_messages=True)
        self.pubsub.subscribe(**self.get_channel_action_mapping())
        self.pubsub_thread = self.pubsub.run_in_thread(sleep_time=0.001)

        # Image handling (filled later if needed)
        self.image_width = 0
        self.image_height = 0
        self.color_space = None

        # Ensure we'll shutdown at some point again
        check_if_alive = Thread(target=self.check_if_alive)
        check_if_alive.start()

    def get_device_types(self):
        return []  # TO IMPLEMENT

    def get_channel_action_mapping(self):
        return {}  # TO IMPLEMENT

    def cleanup(self):
        pass  # TO IMPLEMENT

    def get_full_channel(self, channel_name):
        return self.identifier + '_' + channel_name

    def get_user_id(self):
        return self.identifier.split('-')[0]

    def get_device_id(self):
        return self.identifier.split('-')[1]

    def check_if_alive(self):
        user = 'user:' + self.get_user_id()
        device_id = self.get_device_id()
        devices = []
        for device_type in self.get_device_types():
            devices.append(device_id + ':' + device_type)
        while True:
            try:
                pipe = self.redis.pipeline()
                pipe.time()
                for device in devices:
                    pipe.zscore(user, device)
                results = pipe.execute()

                one_minute = results.pop(0)[0] - 60
                found_one = False
                for score in results:
                    if score >= one_minute:
                        found_one = True
                        break
                if found_one:
                    sleep(60.1)
                    continue
            except:
                pass
            self.shutdown()
            break

    def publish(self, channel, data):
        self.redis.publish(self.get_full_channel(channel), data)

    def produce_event(self, event):
        self.publish('events', event)

    def update_image_properties(self):
        image_size = self.redis.get(self.get_full_channel('image_size')).decode().split()
        self.image_width = int(image_size[0])
        self.image_height = int(image_size[1])
        self.color_space = image_size[2]

    def retrieve_image(self, timestamp, want_stereo=False):
        if self.image_width == 0 or self.image_height == 0:
            self.update_image_properties()

        # Get the raw bytes from Redis
        image_stream = self.redis.zrangebyscore(self.get_full_channel('image_stream'), timestamp, timestamp)
        if not image_stream:
            print('Could not resolve timestamp to image: ' + str(timestamp))
            return (None, None) if want_stereo else None
        raw_img = image_stream[0]

        # Convert the raw bytes to a PIL Image based on the colorspace
        from PIL import Image
        if self.color_space == 'RGB':
            img = Image.frombytes('RGB', (self.image_width, self.image_height), raw_img)
        elif self.color_space == 'YUV':
            # YUV type juggling (end up with YUV444 which is YCbCr which PIL can read directly)
            from numpy import frombuffer, ones, uint8, reshape
            image_array = frombuffer(raw_img, dtype=uint8)
            y = image_array[0::2]
            u = image_array[1::4]
            v = image_array[3::4]
            yuv = ones(len(y) * 3, dtype=uint8)
            yuv[::3] = y
            yuv[1::6] = u
            yuv[2::6] = v
            yuv[4::6] = u
            yuv[5::6] = v
            if len(yuv) == (self.image_height * self.image_width * 3):
                yuv = reshape(yuv, (self.image_height, self.image_width, 3))
                img = Image.fromarray(yuv, 'YCbCr').convert('RGB')
            else:
                print('Actual image size does not match expected size, skipping...')
                return (None, None) if want_stereo else None
        else:
            print('Unsupported color space: ' + self.color_space)
            return (None, None) if want_stereo else None

        left, right = self.split_stereo_img(img)
        if want_stereo:
            return left, right
        else:
            return Image.fromarray(left, 'RGB') if right else img

    @staticmethod
    def split_stereo_img(img):
        from numpy import ndarray, array
        if not isinstance(img, ndarray):
            img = array(img)

        is_stereo_image = (img.shape[1] / img.shape[0]) == STEREO_RATIO
        if is_stereo_image:
            return img[:, :img.shape[1] // 2], img[:, img.shape[1] // 2:]
        else:
            return img, None

    @staticmethod
    def img_to_opencv(img, want_color=True):
        from numpy import ndarray, array
        if not isinstance(img, ndarray):
            img = array(img)

        from cv2 import cvtColor, COLOR_RGB2BGR, COLOR_RGB2GRAY
        return cvtColor(img, COLOR_RGB2BGR if want_color else COLOR_RGB2GRAY)

    @staticmethod
    def load_calibration(calibration_bytes):
        from pickle import loads
        from codecs import decode
        cameramatrix, K, D, H1, H2 = loads(decode(calibration_bytes, 'base64'))

        return cameramatrix, K, D, H1, H2

    @staticmethod
    def rectify_image(calibration_bytes: bytes, left=None, right=None):
        """
        Rectifies and undistorts left and/or right image

        Args:
            calibration_bytes: bytes object: all necessary calibration information
            left: np.ndarray: optional, left image
            right: np.ndarray: optional, right image

        Returns: left and/or right rectified and undistorted image

        """
        newcameramtx, K, D, H1, H2 = SICservice.load_calibration(calibration_bytes)

        def undistort_img(img, image_shape, K, D, cammatrix):
            from cv2 import undistort  # FIXME: magic numbers
            crop_top = int(30 / 360 * image_shape[0])
            crop_left = int(40 / 640 * image_shape[1])
            crop_right = int(-50 / 640 * image_shape[1])
            return undistort(img, K, D, None, cammatrix)[crop_top:, crop_left:crop_right]

        def undistort_and_warp(img):
            from cv2 import warpPerspective
            img = undistort_img(img, img.shape, K, D, newcameramtx)
            return warpPerspective(img, H1, img.shape[::-1])

        def process(img):
            if len(img.shape) == 2:
                left_rectified = undistort_and_warp(img)
            else:
                from numpy import array, moveaxis
                left_rectified = array([undistort_and_warp(img[:, :, i]) for i in range(img.shape[-1])])
                left_rectified = moveaxis(left_rectified, 0, -1)

            return left_rectified

        output = []
        if left is not None:
            output.append(process(left))
        if right is not None:
            output.append(process(right))

        if len(output) == 1:
            output.append(None)

        return output

    def shutdown(self):
        self.cleanup()
        self.running = False
        print('Trying to exit gracefully...')
        try:
            self.pubsub_thread.stop()
            self.redis.close()
            print('Graceful exit was successful')
        except Exception as err:
            print('Graceful exit has failed: ' + err.message)
        self.disconnect(self.identifier)
