from setuptools import setup

setup(
  name='sic_common',
  version='0.0.1',
  author='Koen Hindriks',
  author_email='k.v.hindriks@vu.nl',
  packages=['sic'],
)