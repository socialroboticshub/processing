from sic.factory import SICfactory

from llm_service import LLMService


class LLMFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'llm'

    def create_service(self, connect, identifier, disconnect):
        return LLMService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = LLMFactory()
    factory.run()
