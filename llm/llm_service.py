from openai import OpenAI
from simplejson import loads
from sic.service import SICservice

class LLMService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        super().__init__(connect, identifier, disconnect)

    def get_device_types(self):
        return ['robot']

    def get_channel_action_mapping(self):
        return {
            self.get_full_channel('llm_openai_prompt'): self.openai_prompt
        }

    def openai_prompt(self, message: str) -> None:
        """

        :param message: OpenAI call parameters:
        {
            prompt_id,
            openai_key,
            prompt_params:
            {
                model,
                message,
                temperature (optional)
            }
        }
        """
        try:
            # Parse message to call parameters dictionary
            params = loads(message['data'].decode())
            # Sometimes loads does not work properly. This is the workaround.
            # if not isinstance(prompt_params, dict):
            #     prompt_params = {}
            #     for item in loads(message['data'].decode()):
            #         prompt_params.update(item)

            # Start connection with OpenAI
            client = OpenAI(api_key=params['openai_key'])
            # Prompt LLM with remaining call parameters
            completion = client.chat.completions.create(**params['prompt_params'])
            # Retrieve result
            result = completion.choices[0].message.content
            # Make sure it is compatible with the redis return format.
            result = result.replace(';', ',')

            # Return result as llm_data.
            self.publish('llm_data', f'{params["prompt_id"]};{result}')
        except KeyError as err:
            print(err)
