from sic.factory import SICfactory

from people_detection_service import PeopleDetectionService


class PeopleDetectionFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'people_detection'

    def create_service(self, connect, identifier, disconnect):
        return PeopleDetectionService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = PeopleDetectionFactory()
    factory.run()
