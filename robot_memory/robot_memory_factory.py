from sic.factory import SICfactory

from robot_memory_service import RobotMemoryService


class RobotMemoryFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'robot_memory'

    def create_service(self, connect, identifier, disconnect):
        return RobotMemoryService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = RobotMemoryFactory()
    factory.run()
