var socket = null, audioStream = null, audioCache = [];
$(window).on('load', function() {
	var mainBody = $('#main');
	mainBody.html('Connecting to server...');
	var id = Date.now().toString(36).substring(4) + Math.random().toString(36).substring(2);
	socket = new WebSocket('wss://' + window.location.hostname + ':11881?id=' + id);
	socket.onopen = function() {
		mainBody.html('Connected! ' + id);
	};
	socket.onmessage = function(evt) {
		var data = JSON.parse(evt.data);
		if( data.chan == 'render_html' ) {
			mainBody.html(data.msg);
			updateListeningIcon('ListeningDone');
			vuLogo();
			englishFlag();
			activateButtons();
			chatBox();
			activateSorting();
		} else if( data.chan == 'events' ) {
			updateListeningIcon(data.msg);
		} else if( data.chan == 'text_transcript' ) {
			updateSpeechText(data.msg);
		} else if( data.chan == 'action_audio' ) {
			updateMicrophone(data.msg);
		} else if( data.chan == 'audio_language' ) {
			setTTS(data.msg);
		} else if( data.chan == 'action_say' || data.chan == 'action_say_animated' ) {
			playTTS(data.msg);
		} else if( data.chan == 'action_stop_talking' ) {
			stopTTS();
		} else if( data.chan == 'action_play_audio' ) {
			playAudio(data.msg);
		} else if( data.chan == 'action_load_audio' ) {
			loadAudio(data.msg);
		} else if( data.chan == 'action_clear_loaded_audio' ) {
			clearLoadedAudio();
		} else if( data.chan == 'action_speech_param') {
			if( $('.audioEnabled').length ) socket.send('events|SetSpeechParamDone');
		} else {
			alert(data.chan + ': ' + data.msg);
		}
	};
	socket.onerror = function(err) {
		if( err.message ) alert(err.message);
	};
	socket.onclose = function() {
		mainBody.html('Disconnected');
		if( ttsEl ) stopTTS();
		if( audioStream ) updateMicrophone(-1);
	};
});
$(window).on('unload', function() {
	if( socket ) socket.close();
});
var iconStyle = 'style="height:10vh"';
function updateListeningIcon(input) {
	if( input.startsWith('ListeningStarted') ) {
		$('.listening_icon').html('<img src="img/listening.png" '+iconStyle+'>');
		updateSpeechText(''); // clear it
	} else if( input == 'ListeningDone' ) {
		$('.listening_icon').html('<img src="img/not_listening.png" '+iconStyle+'>');
	}
}
function updateSpeechText(input) {
	$('.speech_text').html(input);
}
function vuLogo() {
	$('.vu_logo').html('<img src="img/vu_logo.png" '+iconStyle+'>');
}
function englishFlag() {
	var englishFlag = $('.english_flag');
	englishFlag.html('<img src="img/english_flag.png" '+iconStyle+'>');
	englishFlag.click(function() {
		socket.send('audio_language|en-US');
		socket.send('dialogflow_language|en-US');
	});
}
function activateButtons() {
	$(':button').click(function() {
		var dataValue = $(this).children().data('value');
		if( dataValue ) {
			socket.send('browser_button|'+dataValue);
		} else {
			var txt = document.createElement('textarea');
			txt.innerHTML = $(this).html();
			socket.send('browser_button|'+txt.value);
		}
	});
	$('form').each(function() {
		var $this = $(this);
		var firstInput = $this.find('input:first');
		firstInput.focus();
		$this.submit(function(e) {
			socket.send('browser_button|'+firstInput.val());
			firstInput.val('');
			e.preventDefault();
		});
	});
}
function chatBox() {
	var chatBox = $('.chatbox');
	chatBox.html('<form><input id="chatbox-input" type="text" class="w-25"><input type="submit"></form>');
	var chatBoxInput = $('#chatbox-input');
	chatBoxInput.focus();
	chatBox.submit(function(e) {
		socket.send('action_chat|'+chatBoxInput.val());
		chatBoxInput.val('');
		e.preventDefault();
	});
}
function activateSorting() {
	var currentSort = [];
	var sortItems = $('.sortitem');
	sortItems.click(function() {
		var $this = $(this);
		var id = $this.attr('id');
		var label = $this.find('.card-text');
		if( currentSort.length > 0 && id == currentSort[currentSort.length-1] ) {
			currentSort.pop();
			label.html('');
		} else if( label.html() == '' ) {
			currentSort.push(id);
			label.html(currentSort.length)
		}
	});
	sortItems.parent().parent().after('<form class="mt-3"><input type="submit" value="Klaar!"></form>');
	$('form').submit(function(e) {
		socket.send('browser_button|'+JSON.stringify(currentSort));
		currentSort = [];
		e.preventDefault();
	});
}
var ttsEl = null, ttsUrl = null, langSet = false;
function setTTS(lang) {
	ttsUrl = '//translate.google.com/translate_tts?ie=UTF-8&client=tw-ob&tl='+lang+'&q=';
	langSet = true;
}
function playTTS(text) {
	if( !$('.audioEnabled').length || !ttsUrl ) return;
	if( langSet ) {
		socket.send('events|LanguageChanged');
		langSet = false;
	}
	if( ttsEl ) stopTTS();
	if( text ) {
		ttsEl = $('<audio></audio>');
		ttsEl.attr('src', ttsUrl+encodeURIComponent(text));
		ttsEl.appendTo('body');
		ttsEl.on('ended', function(){socket.send('events|TextDone')});
   		ttsEl[0].play();
	}
	socket.send('events|TextStarted');
	if( !text ) socket.send('events|TextDone');
}
function stopTTS() {
	if( ttsEl ) {
		ttsEl.remove();
		ttsEl = null;
	}
}
function loadAudio(bytes) {
	if( !$('.audioEnabled').length ) return;
	socket.send('events|LoadAudioStarted');
	audioCache.push(atob(bytes));
	socket.send('robot_audio_loaded|' + (audioCache.length-1));
	socket.send('events|LoadAudioDone');
}
function clearLoadedAudio() {
	if( !$('.audioEnabled').length ) return;
	socket.send('events|ClearAudioStarted');
	audioCache = [];
	socket.send('events|ClearAudioDone');
}
function playAudio(bytesOrIndex) {
	if( !$('.audioEnabled').length ) return;
	var audioIndex = parseFloat(bytesOrIndex);
	if( isNaN(audioIndex) ) bytesOrIndex = atob(bytesOrIndex);
	else bytesOrIndex = audioCache[bytesOrIndex];
	var arrayBuffer = new ArrayBuffer(bytesOrIndex.length);
    var bufferView = new Uint8Array(arrayBuffer);
    for( var i = 0; i < bytesOrIndex.length; i++ ) {
    	bufferView[i] = bytesOrIndex.charCodeAt(i);
    }
	var context = window.AudioContext || window.webkitAudioContext;
	var audioContext = new context();
    audioContext.decodeAudioData(arrayBuffer, function(buffer) {
        var audioSource = audioContext.createBufferSource();
    	audioSource.buffer = buffer;
    	audioSource.connect(audioContext.destination);
    	audioSource.onended = function(){socket.send('events|PlayAudioDone')};
    	audioSource.start(0);
		socket.send('events|PlayAudioStarted');
    });
}
function updateMicrophone(input) {
	if( input >= 0 ) {
		if( !$('.audioEnabled').length ) return;
		if( audioStream ) updateMicrophone(-1);
		audioStream = true;
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
		navigator.getUserMedia({audio: true}, function(stream) {
			audioStream = stream;
	    	var context = window.AudioContext || window.webkitAudioContext;
			var audioContext = new context();
			var sampleRate = audioContext.sampleRate;
			var volume = audioContext.createGain();
			var audioInput = audioContext.createMediaStreamSource(audioStream);
			audioInput.connect(volume);
			var processor = audioContext.createScriptProcessor || audioContext.createJavaScriptNode;
			var recorder = processor.call(audioContext, 2048, 1, 1);
			recorder.onaudioprocess = function(event) {
	   			var PCM32fSamples = event.inputBuffer.getChannelData(0);
				var PCM16iSamples = new ArrayBuffer(PCM32fSamples.length*2);
				var dataView = new DataView(PCM16iSamples);
				for( var i = 0; i < PCM32fSamples.length; i++ ) {
	   				var intVal = Math.max(-32768, Math.min(32767, Math.floor(32767 * PCM32fSamples[i])));
	   				dataView.setInt16(i*2, intVal, true);
				}
				socket.send(PCM16iSamples);
				if( !audioStream ) recorder.disconnect();
			};
			volume.connect(recorder);
			recorder.connect(audioContext.destination);
			socket.send('events|ListeningStarted;1;'+sampleRate);
			if( input > 0 ) setTimeout(function(){updateMicrophone(-1)}, input*1000);
	   }, function(error) {
	       alert(error);
	   });
	} else if( input < 0 && audioStream ) {
		audioStream.getTracks().forEach(function(track){track.stop()});
		audioStream = null;
		socket.send('events|ListeningDone');
	}
}