package org.bitbucket.socialroboticshub;

import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Encoder;

import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoWSD.WebSocket;
import fi.iki.elonen.NanoWSD.WebSocketFrame;
import fi.iki.elonen.NanoWSD.WebSocketFrame.CloseCode;
import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.jedis.Jedis;

public class WebSocketDevice extends SICdevice {
	private final WebSocketServer parent;
	private final WebSocketSIC webSocket;
	private final Jedis listener;
	private final BinaryJedisPubSub pubsub;

	public WebSocketDevice(final WebSocketServer parent, final IHTTPSession session, final String id) throws Exception {
		super(System.getenv("DB_IP"), "default", System.getenv("DB_PASS"), "browser", id);
		this.parent = parent;
		this.webSocket = new WebSocketSIC(session);
		this.listener = connect();
		this.pubsub = new BinaryJedisPubSub() {
			private final Encoder base64 = Base64.getEncoder();

			@Override
			public void onPMessage(final byte[] pattern, final byte[] rawchannel, final byte[] rawmessage) {
				try {
					final String channel = new String(rawchannel, UTF8);
					final int cutoff = channel.indexOf('_') + 1;
					final String topic = channel.substring(cutoff);
					final boolean isBytes = (topic.endsWith("play_audio") || topic.endsWith("load_audio"));
					final String message = isBytes ? this.base64.encodeToString(rawmessage)
							: new String(rawmessage, UTF8);
					WebSocketDevice.this.webSocket.send(topic, message);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		};
	}

	public WebSocket getSocket() {
		return this.webSocket;
	}

	public void closeSocket() throws Exception {
		this.webSocket.close(CloseCode.NormalClosure, "reconnect", true);
	}

	public void run() {
		final String[] patterns = { //
				this.identifier + "_render_html", // html for this specific browser device
				this.identifier + "_audio_language", // audio language commands for the device
				this.identifier + "_action_audio", // audio (recording) commands for the device
				this.identifier + "_action_say", // say commands for the device
				this.identifier + "_action_say_animated", // animated say commands for the device
				this.identifier + "_action_stop_talking", // stop talking commands for the device
				this.identifier + "_action_play_audio", // play audio commands for the device
				this.identifier + "_action_load_audio", // (pre)load audio commands for the device
				this.identifier + "_action_clear_loaded_audio", // clear audio commands for the device
				this.user + "-*_events", // any events of the user's devices
				this.user + "-*_text_transcript", // any transcripts for the user
		};
		final byte[][] rawpatterns = new byte[patterns.length][];
		for (int i = 0; i < patterns.length; i++) {
			rawpatterns[i] = patterns[i].getBytes(UTF8);
		}
		System.out.println("Subscribing '" + this.identifier + "'");
		this.listener.psubscribe(this.pubsub, rawpatterns);
	}

	@Override
	protected void disconnect() {
		super.disconnect();
		this.pubsub.unsubscribe();
		this.listener.close();
		this.parent.removeConnection(this.device);
	}

	private final class WebSocketSIC extends WebSocket {
		WebSocketSIC(final IHTTPSession session) {
			super(session);
		}

		protected void send(final String channel, final String message) throws Exception {
			final String escaped = message.replace("\"", "\\\"");
			final String json = "{\"chan\":\"" + channel + "\",\"msg\":\"" + escaped + "\"}";
			send(json);
		}

		@Override
		protected void onOpen() {
			System.out.println("Opened socket with " + WebSocketDevice.this.identifier);
		}

		@Override
		protected void onMessage(final WebSocketFrame message) {
			if (message.getBinaryPayload().length == 4096) { // assume audio (TODO: improve check)
				stream("audio_stream", message.getBinaryPayload());
			} else {
				final String[] split = message.getTextPayload().split("\\|");
				publish(split[0], (split.length > 1) ? split[1] : "");
			}
		}

		@Override
		protected void onException(final IOException exception) {
			exception.printStackTrace();
		}

		@Override
		protected void onPong(final WebSocketFrame pong) {
		}

		@Override
		protected void onClose(final CloseCode code, final String reason, final boolean initiatedByRemote) {
			System.out.println("Closing socket with " + WebSocketDevice.this.identifier);
			disconnect();
		}
	}
}
