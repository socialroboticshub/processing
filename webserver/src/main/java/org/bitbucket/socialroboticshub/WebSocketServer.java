package org.bitbucket.socialroboticshub;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import fi.iki.elonen.NanoWSD;

final class WebSocketServer extends NanoWSD { // similar to a service factory, but then of devices
	private final Map<String, WebSocketDevice> connections;

	WebSocketServer(final int port) throws Exception {
		super(port);
		this.connections = new ConcurrentHashMap<>();
	}

	public void removeConnection(final String id) {
		this.connections.remove(id);
	}

	@Override
	protected WebSocket openWebSocket(final IHTTPSession session) {
		final List<String> idParam = session.getParameters().get("id");
		if (idParam.isEmpty()) {
			System.err.println("No ID parameter set!");
			return null;
		}
		final String id = idParam.get(0);
		final WebSocketDevice existing = this.connections.get(id);
		if (existing != null) {
			try {
				existing.closeSocket();
			} catch (final Exception ignore) {
			}
		}
		try {
			final WebSocketDevice device = new WebSocketDevice(this, session, id);
			new Thread(() -> {
				device.run();
			}).start();
			this.connections.put(id, device);
			return device.getSocket();
		} catch (final Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
