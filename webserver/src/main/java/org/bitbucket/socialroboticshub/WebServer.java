package org.bitbucket.socialroboticshub;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLServerSocketFactory;

import fi.iki.elonen.NanoHTTPD;

public final class WebServer {
	private static final int httpServerPort = 11880;
	private static final int socketServerPort = 11881;
	private final WebSocketServer webSocketServer;
	private final WebHttpServer webHttpServer;

	public static void main(final String... args) {
		try {
			final WebServer webserver = new WebServer();
			webserver.run();
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private WebServer() throws Exception {
		SSLServerSocketFactory ssl;
		final boolean selfsigned = "1".equals(System.getenv("DB_SSL_SELFSIGNED"));
		final char[] passphrase = "changeit".toCharArray();
		if (selfsigned) {
			ssl = NanoHTTPD.makeSSLSocketFactory("/keystore.jks", passphrase);
		} else {
			final KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			try (final InputStream keystoreStream = new FileInputStream("/etc/ssl/certs/java/keystore.jks")) {
				keystore.load(keystoreStream, passphrase);
			}
			final KeyManagerFactory keyManagerFactory = KeyManagerFactory
					.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keystore, passphrase);
			ssl = NanoHTTPD.makeSSLSocketFactory(keystore, keyManagerFactory);
		}
		this.webHttpServer = new WebHttpServer(httpServerPort);
		this.webHttpServer.makeSecure(ssl, null);
		this.webSocketServer = new WebSocketServer(socketServerPort);
		this.webSocketServer.makeSecure(ssl, null);
	}

	public void run() throws Exception {
		System.out.println("Starting webserver on " + httpServerPort + "...");
		this.webHttpServer.start(0);
		System.out.println("Starting socketserver on " + socketServerPort + "...");
		this.webSocketServer.start(0);
		while (this.webHttpServer.isAlive() || this.webSocketServer.isAlive()) {
			Thread.sleep(1000);
		}
	}
}
