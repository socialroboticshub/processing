package org.bitbucket.socialroboticshub;

public final class StreamVideoServer {
	private static final int streamVideoPort = 11882;
	private final StreamVideo streamVideo;

	public static void main(final String... args) {
		try {
			final StreamVideoServer videoserver = new StreamVideoServer();
			videoserver.run();
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private StreamVideoServer() throws Exception {
		this.streamVideo = new StreamVideo(streamVideoPort);
	}

	public void run() throws Exception {
		System.out.println("Starting video-socketserver on " + streamVideoPort + "...");
		this.streamVideo.run();
	}
}
