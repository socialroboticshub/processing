package org.bitbucket.socialroboticshub;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

/**
 * Handling NaoQI's packed YUV422 format.
 *
 * @author Nicholas Dunn
 * @author Jeremy R. Fishman
 */
public class ImageYUV422 {
	private static final YCbCrColorSpace COLOR_SPACE = new YCbCrColorSpace();

	private static final int BYTES_PER_TWO_PIXELS = 4;
	// Note that we expand the YUV422 image such that every pixel has a Y,U,and
	// V component when we load it from a file, even though the image packs
	// 2 pixels into 4 bytes of data.
	private static final int COLOR_DEPTH = 3;
	private static final int Y1_OFFSET = 0;
	private static final int U_OFFSET = 1;
	private static final int Y2_OFFSET = 2;
	private static final int V_OFFSET = 3;

	private final byte[][][] pixels;
	private final int width;
	private final int height;

	public ImageYUV422(final byte[] rawImage, final int w, final int h) {
		this.width = w;
		this.height = h;
		this.pixels = new byte[h][w][COLOR_DEPTH];
		readByteArray(rawImage);
	}

	// Nao camera outputs in YUV422 format; this means that the YUV information
	// for two adjacent pixels (6 bytes of color info) are packed into 4 bytes
	// Camera output YUV422
	// Byte order Y1, U, Y2, V
	// see http://en.wikipedia.org/wiki/YUV
	private void readByteArray(final byte[] rawImage) {
		int i = 0;
		byte y1, u, y2, v;
		for (int r = 0; r < this.height; r++) {
			for (int c = 0; c < this.width; c += 2, i += BYTES_PER_TWO_PIXELS) {
				y1 = rawImage[i + Y1_OFFSET];
				u = rawImage[i + U_OFFSET];
				y2 = rawImage[i + Y2_OFFSET];
				v = rawImage[i + V_OFFSET];

				this.pixels[r][c][0] = y1;
				this.pixels[r][c][1] = u;
				this.pixels[r][c][2] = v;

				this.pixels[r][c + 1][0] = y2;
				this.pixels[r][c + 1][1] = u;
				this.pixels[r][c + 1][2] = v;
			}
		}
	}

	public BufferedImage createImage() {
		final BufferedImage img = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_RGB);
		final ColorModel cm = img.getColorModel();
		final int[] rgb = new int[3];
		for (int r = 0; r < this.height; r++) {
			for (int c = 0; c < this.width; c++) {
				// Convert YUV to RGB with using the YCbCrColorSpace
				COLOR_SPACE.toRGB(this.pixels[r][c], rgb);
				// Set the BufferedImage pixel to the rgb value of this pixel
				// (use the ColorModel to convert from components to int)
				img.setRGB(c, r, cm.getDataElement(rgb, 0));
			}
		}
		return img;
	}

	/**
	 * A ColorSpace representing the YCbCr color space. Implements the standard
	 * ColorSpace abstract methods toRGB, fromRGB, toCIEXYZ, and fromCIEXYZ, which
	 * all use float arrays to represent the pixels. In addition, this class
	 * provides several handy methods for conversion from and to different types.
	 *
	 * See Charles Poynton's Color FAQ for color technical information about the
	 * color spaces. It is where I got the information for color conversion
	 * matrices, and it is available at http://www.poynton.com/PDFs/ColorFAQ.pdf
	 *
	 * @author Jeremy R. Fishman
	 */
	private static class YCbCrColorSpace extends ColorSpace {
		private static final long serialVersionUID = 1L;

		public YCbCrColorSpace() {
			super(TYPE_YCbCr, 3);
		}

		/**
		 * Convert a 3-pixel RGB [0..+1] float array into a YCbCr [0..256] float array.
		 */
		@Override
		public float[] fromRGB(final float[] rgb) {
			final float[] yCbCr = new float[3];

			yCbCr[0] = 16 + (float) (65.481 * rgb[0] + 128.553 * rgb[1] + 24.966 * rgb[2]);
			yCbCr[1] = 128 + (float) (-37.797 * rgb[0] + -74.203 * rgb[1] + 112. * rgb[2]);
			yCbCr[2] = 128 + (float) (112. * rgb[0] + -93.786 * rgb[1] + -18.214 * rgb[2]);

			for (int i = 0; i < 3; i++) {
				if (yCbCr[i] > 255) {
					yCbCr[i] = 255;
				} else if (yCbCr[i] < 0) {
					yCbCr[i] = 0;
				}
			}

			return yCbCr;
		}

		/**
		 * Convert a 3-pixel YCbCr [0..256] float array into an RGB [0..+1] float array.
		 */
		@Override
		public float[] toRGB(final float[] yCbCr) {
			final float[] rgb = new float[3];
			final float y = yCbCr[0] - 16;
			final float Cb = yCbCr[1] - 128;
			final float Cr = yCbCr[2] - 128;

			rgb[0] = (float) (0.00456621 * y + 0.00625893 * Cr);
			rgb[0] = (float) (0.00456621 * y + -0.00153632 * Cb + -0.00318811 * Cr);
			rgb[0] = (float) (0.00456621 * y + 0.00791071 * Cb);

			for (int i = 0; i < 3; i++) {
				if (rgb[i] > 1) {
					rgb[i] = 1;
				} else if (rgb[i] < 0) {
					rgb[i] = 0;
				}
			}

			return rgb;
		}

		public void toRGB(final byte[] yCbCr, final int[] rgb) {
			toRGB(yCbCr[0] & 0xff, yCbCr[1] & 0xff, yCbCr[2] & 0xff, rgb);
		}

		/**
		 * Convert YCbCr [0..256] int values and place them into an RGB [0..256] int
		 * array.
		 */
		public void toRGB(final int y, final int Cb, final int Cr, final int[] rgb) {
			rgb[0] = ((int) (298.082 * (y - 16) + 408.583 * (Cr - 128))) >> 8;
			rgb[1] = ((int) (298.082 * (y - 16) + -100.291 * (Cb - 128) + -208.120 * (Cr - 128))) >> 8;
			rgb[2] = ((int) (298.082 * (y - 16) + 516.411 * (Cb - 128))) >> 8;

			for (int i = 0; i < 3; i++) {
				if (rgb[i] > 255) {
					rgb[i] = 255;
				} else if (rgb[i] < 0) {
					rgb[i] = 0;
				}
			}

		}

		/**
		 * Convert a 3-pixel CIE XYZ float array into a YCbCr [0..256] float array.
		 */
		@Override
		public float[] fromCIEXYZ(final float[] cie_xyz) {
			final float[] rgb = new float[3];

			rgb[0] = (float) (3.240479 * cie_xyz[0] + -1.537150 * cie_xyz[1] + -0.498535 * cie_xyz[2]);
			rgb[1] = (float) (-0.969256 * cie_xyz[0] + 1.875992 * cie_xyz[1] + 0.041556 * cie_xyz[2]);
			rgb[2] = (float) (0.055648 * cie_xyz[0] + -0.204043 * cie_xyz[1] + 1.057311 * cie_xyz[2]);

			return fromRGB(rgb);
		}

		/**
		 * Convert a 3-pixel YCbCr [0..256] float array into a CIE XYZ float array.
		 */
		@Override
		public float[] toCIEXYZ(final float[] yCbCr) {
			final float[] rgb = toRGB(yCbCr);
			final float[] cie_xyz = new float[3];

			cie_xyz[0] = (float) (0.412453 * rgb[0] + 0.357580 * rgb[1] + 0.180423 * rgb[2]);
			cie_xyz[1] = (float) (0.212671 * rgb[0] + 0.715160 * rgb[1] + 0.072169 * rgb[2]);
			cie_xyz[2] = (float) (0.019334 * rgb[0] + 0.119193 * rgb[1] + 0.950227 * rgb[2]);

			return cie_xyz;
		}
	}
}