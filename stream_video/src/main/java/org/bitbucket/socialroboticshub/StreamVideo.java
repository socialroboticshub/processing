package org.bitbucket.socialroboticshub;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.imageio.ImageIO;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.Protocol;

final class StreamVideo extends WebSocketServer {
	private final Map<String, WebSocket> connections;
	private final Map<String, SocketListener> listeners;

	StreamVideo(final int port) throws Exception {
		super(new InetSocketAddress(port));
		this.connections = new ConcurrentHashMap<>();
		this.listeners = new ConcurrentHashMap<>();
	}

	@Override
	public void onOpen(final WebSocket conn, final ClientHandshake handshake) {
	}

	@Override
	public void onClose(final WebSocket conn, final int code, final String reason, final boolean remote) {
		final Iterator<Entry<String, WebSocket>> iterator = this.connections.entrySet().iterator();
		while (iterator.hasNext()) {
			final Entry<String, WebSocket> connection = iterator.next();
			if (connection.getValue() == conn) { // direct == should work here
				final String identifier = connection.getKey();
				System.out.println("Closing video-socket with '" + identifier + "'...");
				this.connections.remove(identifier);
				this.listeners.get(identifier).disconnect();
				break;
			}
		}
	}

	@Override
	public void onMessage(final WebSocket conn, final String message) {
		final WebSocket existing = this.connections.get(message);
		if (existing != null) {
			try {
				existing.close(1000, "reconnect");
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		this.connections.put(message, conn);
		if (!this.listeners.containsKey(message)) {
			try {
				final SocketListener listener = new SocketListener(message);
				listener.start();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onMessage(final WebSocket conn, final ByteBuffer message) {
		onMessage(conn, StandardCharsets.UTF_8.decode(message).toString());
	}

	@Override
	public void onError(final WebSocket conn, final Exception ex) {
		ex.printStackTrace();
	}

	@Override
	public void onStart() {
	}

	private final class SocketListener extends Thread {
		private final String identifier;
		private final byte[] videostreamtopic;
		private final Jedis subscriber, publisher;
		private final JedisPubSub pubsub;
		private int width, height;
		private String colorSpace;

		SocketListener(final String identifier) throws Exception {
			this.identifier = identifier;
			this.videostreamtopic = (identifier + "_image_stream").getBytes(StandardCharsets.UTF_8);
			this.publisher = connect();
			this.subscriber = connect();
			this.pubsub = new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					try {
						send(getImage());
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			};
			StreamVideo.this.listeners.put(identifier, this);
		}

		public void disconnect() {
			this.pubsub.unsubscribe();
			this.subscriber.close();
			this.publisher.close();
			StreamVideo.this.listeners.remove(this.identifier);
		}

		private void send(final byte[] payload) throws Exception {
			final WebSocket socket = StreamVideo.this.connections.get(this.identifier);
			if (socket != null) {
				socket.send(payload);
			}
		}

		@Override
		public void run() {
			System.out.println("Subscribing '" + this.identifier + "'");
			this.subscriber.subscribe(this.pubsub, this.identifier + "_image_available");
		}

		private byte[] getImage() throws Exception {
			final byte[] data = this.publisher.zrevrange(this.videostreamtopic, 0, 0).get(0);
			if (this.width == 0 || this.height == 0) {
				final String imgsize[] = this.publisher.get(this.identifier + "_image_size").split(" ");
				this.width = Integer.parseInt(imgsize[0]);
				this.height = Integer.parseInt(imgsize[1]);
				this.colorSpace = imgsize[2];
			}
			final BufferedImage img = switch (this.colorSpace) {
			case "RGB" -> new ImageRGB(data, this.width, this.height).createImage();
			case "YUV" -> new ImageYUV422(data, this.width, this.height).createImage();
			default -> throw new Exception("Unknown color space: " + this.colorSpace);
			};
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(img, "jpg", baos);
			return baos.toByteArray();
		}
	}

	private Jedis connect() throws Exception {
		Jedis jedis;
		final boolean selfsigned = "1".equals(System.getenv("DB_SSL_SELFSIGNED"));
		if (selfsigned) {
			final KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(StreamVideo.class.getResourceAsStream("/truststore.jks"), "changeit".toCharArray());
			final Certificate original = ((KeyStore.TrustedCertificateEntry) keyStore.getEntry("sic", null))
					.getTrustedCertificate();
			final TrustManager bypass = new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[] { (X509Certificate) original };
				}

				@Override
				public void checkClientTrusted(final X509Certificate[] chain, final String authType)
						throws CertificateException {
					checkServerTrusted(chain, authType);
				}

				@Override
				public void checkServerTrusted(final X509Certificate[] chain, final String authType)
						throws CertificateException {
					if (chain.length != 1 || !chain[0].equals(original)) {
						throw new CertificateException("Invalid certificate provided");
					}
				}
			};
			final SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[] { bypass }, null);
			final SSLSocketFactory sslFactory = sslContext.getSocketFactory();
			jedis = new Jedis(System.getenv("DB_IP"), Protocol.DEFAULT_PORT, true, sslFactory, null, null);
		} else {
			jedis = new Jedis(System.getenv("DB_IP"), Protocol.DEFAULT_PORT, true);
		}
		jedis.auth(System.getenv("DB_PASS"));
		return jedis;
	}
}