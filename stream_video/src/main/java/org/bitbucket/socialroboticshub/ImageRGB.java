package org.bitbucket.socialroboticshub;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;

/**
 * Handling standard RGB images.
 *
 * @author Northern Bites Team
 * @author http://robocup.bowdoin.edu/blog/history
 */
public class ImageRGB {
	private static final int COLOR_DEPTH = 3;

	private final byte[][][] pixels;
	private final int width;
	private final int height;

	public ImageRGB(final byte[] rawImage, final int w, final int h) {
		this.width = w;
		this.height = h;
		this.pixels = new byte[h][w][COLOR_DEPTH];
		readByteArray(rawImage);
	}

	public void readByteArray(final byte[] rawImage) {
		int i = 0;
		for (int r = 0; r < this.height; r++) {
			for (int c = 0; c < this.width; c++) {
				this.pixels[r][c][0] = rawImage[i++];
				this.pixels[r][c][1] = rawImage[i++];
				this.pixels[r][c][2] = rawImage[i++];
			}
		}
	}

	public BufferedImage createImage() {
		final BufferedImage img = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_RGB);
		final ColorModel cm = img.getColorModel();
		final int[] rgb = new int[3];
		for (int r = 0; r < this.height; r++) {
			for (int c = 0; c < this.width; c++) {
				// Fill the integer rgb array with byte values
				rgb[0] = this.pixels[r][c][0];
				rgb[1] = this.pixels[r][c][1];
				rgb[2] = this.pixels[r][c][2];
				// Set the BufferedImage pixel to the rgb value of this pixel
				// (use the ColorModel to convert from components to int)
				img.setRGB(c, r, cm.getDataElement(rgb, 0));
			}
		}
		return img;
	}
}