#!/bin/bash

echo Building computer_common...
cd ../input/computer_common
mvn clean install -q

echo Deploying robot_scripts_installer...
cd ../robot_scripts_installer
mvn clean package -q
cp -f target/robot-scripts-installer-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/robot-installer.jar

echo Deploying computer_camera...
cd ../computer_camera
mvn clean package -q
cp -f target/computer-camera-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/computer-camera.jar

echo Deploying computer_microphone...
cd ../computer_microphone
mvn clean package -q
cp -f target/computer-microphone-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/computer-microphone.jar

echo Deploying google_assistant...
cd ../google_assistant
mvn clean package -q
cp -f target/computer-google-assistant-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/google-assistant.jar

echo Deploying computer_browser...
cd ../../output/computer_browser
mvn clean package -q
cp -f target/computer-browser-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/computer-browser.jar

echo Deploying computer_robot...
cd ../computer_robot
mvn clean package -q
cp -f target/computer-robot-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/computer-robot.jar

echo Deploying computer_speakers...
cd ../computer_speakers
mvn clean package -q
cp -f target/computer-speakers-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/computer-speakers.jar

echo Deploying logger...
cd ../logger
mvn clean package -q
cp -f target/logger-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic-local/logger.jar

cd ../../processing
echo Done!
