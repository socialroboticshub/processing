from sic.factory import SICfactory

from face_recognition_service import FaceRecognitionService


class FaceRecognitionFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'face_recognition'

    def create_service(self, connect, identifier, disconnect):
        return FaceRecognitionService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = FaceRecognitionFactory()
    factory.run()
