import face_recognition
from cv2 import createBackgroundSubtractorMOG2, LUT
from numpy import arange, argmin, array
from os.path import isfile
from pickle import load, dump
from sic.service import SICservice
from threading import Event, Thread


class FaceRecognitionService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        self.is_recognizing = False
        self.save_image = False
        self.img_timestamp = None
        self.img_available = Event()

        # Initialize face recognition data
        self.face_labels = []
        self.face_names = []
        self.face_count = []
        self.face_encoding_path = 'face_encodings.p3'
        if not isfile(self.face_encoding_path):
            dump_channel = open(self.face_encoding_path, 'wb')
            dump([], dump_channel)
            dump_channel.close()
        load_channel = open(self.face_encoding_path, 'rb')
        self.face_encodings_list = load(load_channel)
        load_channel.close()
        # Create a difference between background and foreground image
        self.fgbg = createBackgroundSubtractorMOG2()

        super().__init__(connect, identifier, disconnect)

    def get_device_types(self):
        return ['cam']

    def get_channel_action_mapping(self):
        return {self.get_full_channel('events'): self.execute,
                self.get_full_channel('image_available'): self.set_image_available,
                self.get_full_channel('action_take_picture'): self.take_picture}

    def execute(self, message):
        data = message['data'].decode()
        if data == 'WatchingStarted':
            if not self.is_recognizing:
                self.is_recognizing = True
                self.update_image_properties()
                face_recognition_thread = Thread(target=self.recognize_face)
                face_recognition_thread.start()
            else:
                print('Face recognition already running for ' + self.identifier)
        elif data == 'WatchingDone':
            if self.is_recognizing:
                self.is_recognizing = False
                self.img_available.set()
            else:
                print('Face recognition already stopped for ' + self.identifier)

    def recognize_face(self):
        self.produce_event('FaceRecognitionStarted')
        while self.is_recognizing:
            if self.img_available.is_set():
                timestamp = self.img_timestamp
                if timestamp is None:
                    continue
                image = self.retrieve_image(timestamp)
                self.img_available.clear()
                if image is None:
                    continue

                process_image = array(image)[:, :, ::-1]

                # Manipulate process_image in order to help face recognition
                # self.normalise_luminescence(process_image) FIXME: gives error?!
                self.fgbg.apply(process_image)

                face_locations = face_recognition.face_locations(process_image, model='hog')
                face_encodings = face_recognition.face_encodings(process_image, face_locations)
                face_name = []
                for face_encoding in face_encodings:
                    match = face_recognition.compare_faces(self.face_encodings_list, face_encoding, tolerance=0.6)
                    dist = face_recognition.face_distance(self.face_encodings_list, face_encoding)
                    if all(values == False for values in match) and all([d for d in dist if d > 0.7]):
                        count = len(self.face_encodings_list)
                        name = str(count)
                        self.face_count.append(count)
                        self.face_encodings_list.append(face_encoding)
                        self.face_names.append(name)
                        dump(self.face_encodings_list, open(self.face_encoding_path, 'wb'))
                        print(self.identifier + ': New face recognised (' + name + ')')
                    else:
                        index = match.index(True)
                        tmp = str(index)
                        if index == argmin(dist):
                            name = tmp
                            face_name.append(name)
                            print(self.identifier + ': Recognised existing face (' + name + ')')
                        else:
                            print(self.identifier + ': Mismatch in recognition')
                            continue
                    self.publish('recognised_face', name)
                else:
                    self.img_available.wait()
        self.produce_event('FaceRecognitionDone')

    def set_image_available(self, message):
        raw_timestamp = message['data'].decode()
        self.img_timestamp = int(raw_timestamp)
        self.img_available.set()

    def take_picture(self, message):
        self.save_image = True

    @staticmethod
    def normalise_luminescence(image, gamma=2.5):
        # build a lookup table mapping the pixel values [0, 255] to
        # their adjusted gamma values such that any image has the same luminescence
        inv_gamma = 1.0 / gamma
        table = array([((i / 255.0) ** inv_gamma) * 255
                       for i in arange(0, 256)]).astype(uint8)

        # apply gamma correction using the lookup table
        return LUT(image, table, image)

    def cleanup(self):
        self.is_recognizing = False
        self.img_available.set()
