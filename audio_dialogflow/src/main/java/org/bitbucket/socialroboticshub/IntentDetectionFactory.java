package org.bitbucket.socialroboticshub;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

public final class IntentDetectionFactory extends SICfactory {
	private final static String PROFILING = "p";
	private final boolean profiling;

	public static void main(final String... args) {
		try {
			final Options opts = new Options().addOption(PROFILING, false, "enable profiling");
			final CommandLine cmd = new DefaultParser().parse(opts, args);
			final boolean profiling = cmd.hasOption(PROFILING);
			final IntentDetectionFactory factory = new IntentDetectionFactory(profiling);
			factory.run();
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private IntentDetectionFactory(final boolean profiling) {
		this.profiling = profiling;
	}

	public boolean shouldProfile() {
		return this.profiling;
	}

	@Override
	protected String getConnectionChannel() {
		return "intent_detection";
	}

	@Override
	protected SICservice createService(final String identifier) throws Exception {
		return new IntentDetection(this, identifier);
	}
}
