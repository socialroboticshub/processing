package org.bitbucket.socialroboticshub;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bitbucket.socialroboticshub.DetectionResultProto.DetectionResult;

import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.api.gax.rpc.StreamController;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.dialogflow.v2.AudioEncoding;
import com.google.cloud.dialogflow.v2.Context;
import com.google.cloud.dialogflow.v2.DetectIntentRequest;
import com.google.cloud.dialogflow.v2.DetectIntentResponse;
import com.google.cloud.dialogflow.v2.InputAudioConfig;
import com.google.cloud.dialogflow.v2.QueryInput;
import com.google.cloud.dialogflow.v2.QueryParameters;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.cloud.dialogflow.v2.SessionsClient;
import com.google.cloud.dialogflow.v2.SessionsSettings;
import com.google.cloud.dialogflow.v2.StreamingDetectIntentRequest;
import com.google.cloud.dialogflow.v2.StreamingDetectIntentResponse;
import com.google.cloud.dialogflow.v2.StreamingRecognitionResult;
import com.google.cloud.dialogflow.v2.TextInput;
import com.google.protobuf.ByteString;
import com.google.protobuf.Value;

import redis.clients.jedis.Jedis;

final class IntentDetection extends SICservice {
	private static final AudioEncoding audioEncoding = AudioEncoding.AUDIO_ENCODING_LINEAR_16;
	private final Profiler profiler;
	private final byte[] intenttopic;
	private final byte[] audiorecordingtopic;
	private SessionsClient client;
	private SessionName session;
	private String language;
	private String context;
	private RedisAudioIterator audio;
	private volatile boolean shouldRecord;

	IntentDetection(final IntentDetectionFactory parent, final String identifier) throws Exception {
		super(parent, identifier);
		this.profiler = new Profiler(parent.shouldProfile(), identifier);
		this.profiler.start();
		this.intenttopic = stringToBytes(getFullChannel("audio_intent"));
		this.audiorecordingtopic = stringToBytes(getFullChannel("audio_newfile"));
	}

	@Override
	protected DeviceType[] getDeviceTypes() {
		return new DeviceType[] { DeviceType.MICROPHONE, DeviceType.BROWSER }; // audio+chat
	}

	@Override
	protected String[] getSubscriptions() {
		return new String[] { "dialogflow_language", "dialogflow_context", "dialogflow_key", "dialogflow_agent",
				"dialogflow_record", "events", "action_chat", "audio_channels" };
	}

	private void setKey(final byte[] key) throws Exception {
		final InputStream stream = new ByteArrayInputStream(key);
		final ServiceAccountCredentials credentials = ServiceAccountCredentials.fromStream(stream);
		final SessionsSettings settings = SessionsSettings.newBuilder()
				.setCredentialsProvider(FixedCredentialsProvider.create(credentials)).build();
		this.client = SessionsClient.create(settings);
	}

	private void setAgent(final byte[] agent) {
		this.session = SessionName.of(bytesToString(agent), UUID.randomUUID().toString());
	}

	private void setLanguage(final byte[] language) {
		this.language = bytesToString(language);
	}

	private void setContext(final byte[] context) {
		this.context = bytesToString(context);
	}

	@Override
	protected void close() {
		if (this.audio != null) {
			this.audio.close();
			this.audio = null;
		}
		super.close();
	}

	@Override
	protected void onMessage(final String channel, final byte[] data) throws Exception {
		switch (channel) {
		case "dialogflow_language":
			setLanguage(data);
			break;
		case "dialogflow_context":
			setContext(data);
			break;
		case "dialogflow_key":
			setKey(data);
			break;
		case "dialogflow_agent":
			setAgent(data);
			break;
		case "dialogflow_record":
			IntentDetection.this.shouldRecord = "1".equals(bytesToString(data));
			break;
		case "events":
			final String event = bytesToString(data);
			if (event.startsWith("ListeningStarted")) {
				final String[] audioData = event.split(";");
				if (IntentDetection.this.audio == null) {
					IntentDetection.this.audio = new RedisAudioIterator(Integer.parseInt(audioData[2]));
					new Thread(() -> {
						detectIntent(IntentDetection.this.audio);
					}).start();
				} else {
					System.err.println(IntentDetection.this.identifier + " already listening...");
				}
			} else if (event.equals("ListeningDone")) {
				if (IntentDetection.this.audio != null) {
					IntentDetection.this.audio.close();
					IntentDetection.this.audio = null;
				} else {
					System.err.println(IntentDetection.this.identifier + " already not listening...");
				}
			}
			break;
		case "action_chat":
			final String text = bytesToString(data);
			new Thread(() -> {
				detectIntent(text);
			}).start();
			break;
		}
	}

	/**
	 * @param audio The audio input
	 *
	 * @return Using the set agent and language (and optionally a context), keep
	 *         trying to match intents whenever something is available from the
	 *         audio input. We abort after the first match (single utterance).
	 */
	private void detectIntent(final RedisAudioIterator audio) {
		// Instructs the speech recognizer how to process the audio content
		final ResponseObserver<StreamingDetectIntentResponse> responseObserver = new ResponseObserver<>() {
			@Override
			public void onStart(final StreamController controller) {
				System.out.println("START INTENT DETECTION FOR " + IntentDetection.this.identifier);
				produceEvent("IntentDetectionStarted");
			}

			@Override
			public void onResponse(final StreamingDetectIntentResponse response) {
				processRecognisedIntent(response.getQueryResult(), "audio", response.getRecognitionResult());
			}

			@Override
			public void onError(final Throwable t) {
				t.printStackTrace();
			}

			@Override
			public void onComplete() {
				try {
					((Closeable) audio).close();
				} catch (final Exception e) {
					onError(e);
				} finally {
					produceEvent("IntentDetectionDone");
					System.out.println("STOPPED INTENT DETECTION FOR " + IntentDetection.this.identifier);
					setContext(null);
				}
			}
		};

		// Basic checks
		try {
			verifyParametersSet();
		} catch (final Exception e) {
			System.err.println(e.getMessage() + ", aborting intent detection for " + this.identifier + "...");
			responseObserver.onComplete();
			return;
		}

		// Performs the streaming detect intent callable request
		final ClientStream<StreamingDetectIntentRequest> requestObserver = this.client.streamingDetectIntentCallable()
				.splitCall(responseObserver);
		// Build the query with an InputAudioConfig
		final InputAudioConfig.Builder inputAudioConfig = InputAudioConfig.newBuilder().setAudioEncoding(audioEncoding)
				.setLanguageCode(this.language).setSampleRateHertz(audio.getSampleRate()).setSingleUtterance(true);
		final QueryInput.Builder queryInput = QueryInput.newBuilder().setAudioConfig(inputAudioConfig);

		try {
			// The first request contains the configuration
			final Long initStart = this.profiler.start();
			final StreamingDetectIntentRequest init = StreamingDetectIntentRequest.newBuilder()
					.setSession(this.session.toString()).setQueryParams(getQueryParametersWithContext())
					.setQueryInput(queryInput).build();
			requestObserver.send(init);
			this.profiler.end("INIT", initStart);
			final StreamingDetectIntentRequest.Builder builder = StreamingDetectIntentRequest.newBuilder();
			while (audio.hasNext()) {
				final byte[] next = audio.next(); // this is a blocking call
				if (next != null) {
					final Long sendStart = this.profiler.start();
					final ByteString chunk = ByteString.copyFrom(next);
					final StreamingDetectIntentRequest request = builder.setInputAudio(chunk).build();
					requestObserver.send(request);
					this.profiler.end("SEND", sendStart);
				}
			}
			// Half-close the stream
			requestObserver.closeSend();
		} catch (final Throwable e) {
			// Cancel stream
			requestObserver.closeSendWithError(e);
		}
	}

	/**
	 * @param text A string
	 *
	 * @return Using the set agent and language (and optionally a context), try to
	 *         match an intent on the given text.
	 */
	private void detectIntent(final String text) {
		try {
			verifyParametersSet();
		} catch (final Exception e) {
			System.err.println(e.getMessage() + ", aborting intent detection for " + this.identifier + "...");
			return;
		}

		final TextInput.Builder textInput = TextInput.newBuilder().setLanguageCode(this.language).setText(text);
		final QueryInput.Builder queryInput = QueryInput.newBuilder().setText(textInput);
		final DetectIntentRequest.Builder detectIntent = DetectIntentRequest.newBuilder()
				.setSession(this.session.toString()).setQueryParams(getQueryParametersWithContext())
				.setQueryInput(queryInput);
		final DetectIntentResponse response = this.client.detectIntent(detectIntent.build());
		processRecognisedIntent(response.getQueryResult(), "chat");
	}

	private void verifyParametersSet() throws Exception {
		if (this.client == null) {
			throw new Exception("No keyfile set");
		} else if (this.session == null) {
			throw new Exception("No agent set");
		} else if (this.language == null) {
			throw new Exception("No language set");
		}
	}

	private QueryParameters.Builder getQueryParametersWithContext() {
		final QueryParameters.Builder queryParams = QueryParameters.newBuilder();
		if (this.context != null && !this.context.isEmpty()) {
			System.out.println("CONTEXT FOR " + this.identifier + ": " + this.context);
			final String name = this.session + "/contexts/" + this.context;
			queryParams.addContexts(Context.newBuilder().setName(name).setLifespanCount(1));
		}
		return queryParams;
	}

	public void processRecognisedIntent(final QueryResult intent, final String source) {
		processRecognisedIntent(intent, source, null);
	}

	private void processRecognisedIntent(final QueryResult intent, final String source,
			final StreamingRecognitionResult stream) {
		final DetectionResult.Builder result = DetectionResult.newBuilder();
		result.setSource(source);

		final int confidence = (int) (intent.getIntentDetectionConfidence() * 100);
		if (confidence > 0) { // an intent was detected
			final String name = intent.getIntent().getDisplayName();
			System.out.format("%s > Detected Intent: '%s' (confidence %d%%)\n", this.identifier, name, confidence);
			result.setIntent(intent.getAction());
			final Map<String, Value> parameters = intent.getParameters().getFieldsMap();
			result.putAllParameters(parameters);
			if (!parameters.isEmpty()) {
				System.out.println(parameters);
			}
			result.setConfidence(confidence);
		}

		final String queryText = intent.getQueryText();
		if (!queryText.isEmpty()) { // the final recognised text (if any)
			System.out.format("%s > Recognised Text: '%s'\n", this.identifier, queryText);
			result.setText(queryText);
		}

		if (confidence > 0 || !queryText.isEmpty()) {
			this.publisher.publish(this.intenttopic, result.build().toByteArray());
		} else {
			final String transcript = (stream == null) ? "" : stream.getTranscript();
			if (!transcript.isEmpty()) { // publish what we heard so far (if anything)
				System.out.format("%s > Transcript: '%s'\n", this.identifier, transcript);
				this.publisher.publish(this.identifier + "_text_transcript", transcript);
			}
		}
	}

	private final class RedisAudioIterator implements Iterator<byte[]>, Closeable {
		private final Jedis redis;
		private final int sampleRate;
		private final byte[] audiostreamtopic;
		private final List<byte[]> allbytes;
		private int totalSize = 0;
		private volatile boolean closed = false;

		RedisAudioIterator(final int sampleRate) throws Exception {
			this.redis = IntentDetection.this.parent.connect();
			this.sampleRate = sampleRate;
			this.audiostreamtopic = stringToBytes(getFullChannel("audio_stream"));
			this.allbytes = IntentDetection.this.shouldRecord ? new ArrayList<>() : null;
		}

		public int getSampleRate() {
			return this.sampleRate;
		}

		@Override
		public boolean hasNext() {
			if (this.closed) {
				this.redis.close();
				return false;
			} else {
				return true;
			}
		}

		@Override
		public byte[] next() {
			byte[] next;
			do {
				next = this.redis.lpop(this.audiostreamtopic);
				if (next == null) { // nothing yet; wait
					try {
						Thread.sleep(1);
					} catch (final InterruptedException ignore) {
					}
				}
			} while (next == null && !this.closed);
			if (next != null && this.allbytes != null) {
				this.allbytes.add(next);
				this.totalSize += next.length;
			}
			return next;
		}

		@Override
		public void close() {
			if (!this.closed) {
				this.closed = true;
				if (this.allbytes != null) {
					try {
						final ByteBuffer data = ByteBuffer.allocate(this.totalSize);
						for (final byte[] chunk : this.allbytes) {
							normalizeVolume(chunk);
							data.put(chunk);
						}
						publishAudio(data.order(ByteOrder.LITTLE_ENDIAN).rewind(), this.sampleRate);
					} catch (final Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	private void publishAudio(final ByteBuffer data, final int sampleRate) throws Exception {
		final byte[] recording = PCMtoWav(data, sampleRate);
		this.publisher.publish(this.audiorecordingtopic, recording);
	}

	private static byte[] PCMtoWav(final ByteBuffer data, final int sampleRate) throws Exception {
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		writeString(output, "RIFF"); // chunk id
		writeInt(output, 36 + data.capacity()); // chunk size
		writeString(output, "WAVE"); // format
		writeString(output, "fmt "); // subchunk 1 id
		writeInt(output, 16); // subchunk 1 size
		writeShort(output, (short) 1); // audio format (1 = PCM)
		writeShort(output, (short) 1); // number of channels
		writeInt(output, sampleRate); // sample rate
		writeInt(output, sampleRate * 2); // byte rate (samplerate*channels*bits/8)
		writeShort(output, (short) 2); // block align (channels*bits/8)
		writeShort(output, (short) 16); // bits per sample
		writeString(output, "data"); // subchunk 2 id
		writeInt(output, data.capacity()); // subchunk 2 size
		final short[] shorts = new short[data.capacity() / 2];
		data.asShortBuffer().get(shorts);
		for (final short s : shorts) {
			writeShort(output, s);
		}
		return output.toByteArray();
	}

	private static void writeInt(final OutputStream output, final int value) throws Exception {
		output.write(value >> 0);
		output.write(value >> 8);
		output.write(value >> 16);
		output.write(value >> 24);
	}

	private static void writeShort(final OutputStream output, final short value) throws Exception {
		output.write(value >> 0);
		output.write(value >> 8);
	}

	private static void writeString(final OutputStream output, final String value) throws Exception {
		for (int i = 0; i < value.length(); i++) {
			output.write(value.charAt(i));
		}
	}

	private static int N_SHORTS = 0xffff;
	private static final short[] VOLUME_NORM_LUT = new short[N_SHORTS];
	private static int MAX_NEGATIVE_AMPLITUDE = 0x8000;
	static { // https://stackoverflow.com/a/52683924/787926
		for (int s = 0; s < N_SHORTS; s++) {
			final double v = (s - MAX_NEGATIVE_AMPLITUDE);
			final double sign = Math.signum(v);
			VOLUME_NORM_LUT[s] = (short) sign;
			VOLUME_NORM_LUT[s] *= (1.240769e-22 - (-4.66022 / 0.0001408133) * (1 - Math.exp(-0.0001408133 * v * sign)));
		}
	}

	private static void normalizeVolume(final byte[] audioSamples) {
		for (int i = 0; i < audioSamples.length; i += 2) {
			short s1 = audioSamples[i + 1];
			short s2 = audioSamples[i];
			s1 = (short) ((s1 & 0xff) << 8);
			s2 = (short) (s2 & 0xff);
			short res = (short) (s1 | s2);
			res = VOLUME_NORM_LUT[Math.min(res + MAX_NEGATIVE_AMPLITUDE, N_SHORTS - 1)];
			audioSamples[i] = (byte) res;
			audioSamples[i + 1] = (byte) (res >> 8);
		}
	}
}
