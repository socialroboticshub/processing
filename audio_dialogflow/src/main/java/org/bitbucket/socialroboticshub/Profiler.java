package org.bitbucket.socialroboticshub;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

public class Profiler {
	private volatile boolean enabled;
	private final String name;
	private final Map<String, Long> cache;
	private final BlockingQueue<String> queue;

	public Profiler(final boolean enabled, final String identifier) {
		this.enabled = enabled;
		this.name = enabled ? (identifier + "_" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date())) : null;
		this.cache = enabled ? new ConcurrentHashMap<>() : null;
		this.queue = enabled ? new LinkedBlockingQueue<>() : null;
		if (enabled) {
			new Thread(() -> {
				write();
			}).start();
		}
	}

	public Long start() {
		return this.enabled ? System.nanoTime() : null;
	}

	public void end(final String label, final Long start) {
		if (this.enabled) {
			final double diff = (System.nanoTime() - start.longValue()) / 1000000.0;
			final String info = (label + ";" + String.format("%.1f", diff) + "\n");
			this.queue.add(info);
		}
	}

	private void write() {
		FileWriter writer = null;
		while (this.enabled) {
			try {
				final String next = this.queue.take();
				if (writer == null) {
					writer = new FileWriter(this.name + ".csv");
				}
				writer.write(next);
				writer.flush();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		if (writer != null) {
			try {
				writer.close();
			} catch (final Exception ignore) {
			}
		}
	}

	public void shutdown() {
		if (this.enabled) {
			this.cache.clear();
			this.queue.add("END;");
			this.enabled = false;
		}
	}
}
