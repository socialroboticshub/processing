#!/bin/bash

echo Building common_java...
cd common_java
mvn clean install -q

echo Copying common_python...
cd ../common_python
cp -f setup.py ../../docker/sic/common_python
cp -rf sic ../../docker/sic/common_python/

echo Copying beamforming...
cd ../audio_beamforming
cp -f {cert.pem,*.py,psd_fan_noise.npy} ../../docker/sic/beamforming
cp -rf {multi_microphone,single_microphone,tools}  ../../docker/sic/beamforming/

echo Deploying dialogflow...
cd ../audio_dialogflow
mvn clean package -q
cp -f target/audio-dialogflow-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic/dialogflow/audio-dialogflow.jar

echo Deploying sentiment_analysis...
cd ../audio_sentiment
cp -f {cert.pem,classifier.pickle,*.py} ../../docker/sic/sentiment

echo Copying robot_memory...
cd ../robot_memory
cp -f {cert.pem,*.py} ../../docker/sic/robot_memory

echo Copying text_to_speech...
cd ../text_to_speech
cp -f {cert.pem,*.py} ../../docker/sic/text_to_speech

echo Deploying stream_audio...
cd ../stream_audio
mvn clean package -q
cp -f target/stream-audio-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic/stream_audio/stream-audio.jar

echo Deploying stream_video...
cd ../stream_video
mvn clean package -q
cp -f target/stream-video-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic/stream_video/stream-video.jar

echo Copying video_coronacheck...
cd ../video_coronacheck
cp -f {cert.pem,*.py} ../../docker/sic/corona_check

echo Copying video_emotion...
cd ../video_emotion
cp -f {cert.pem,emotion_model.hdf5,*.py} ../../docker/sic/emotion_detection
cp -rf utils ../../docker/sic/emotion_detection/

echo Copying video_facerecognition...
cd ../video_facerecognition
cp -f {cert.pem,*.py} ../../docker/sic/face_recognition

echo Copying video_peopledetection...
cd ../video_peopledetection
cp -f {cert.pem,*.py} ../../docker/sic/people_detection

echo Copying video_depth_estimation...
cd ../video_depth_estimation
cp -f {cert.pem,*.py} ../../docker/sic/depth_estimation
cd
echo Copying video_object_detection...
cd ../video_object_detection
cp -f {cert.pem,*.py,*.pkl} ../../docker/sic/object_detection

echo Copying video_object_tracking...
cd ../video_object_tracking
cp -f {cert.pem,*.py} ../../docker/sic/object_tracking

echo Deploying webserver...
cd ../webserver
mvn clean package -q
cp -f target/webserver-0.0.1-SNAPSHOT-shaded.jar ../../docker/sic/webserver/webserver.jar
cp -rf html ../../docker/sic/webserver/

echo Collecting robot scripts...
cd ../../input
cp robot_camera/*.py ../docker/sic/robot_scripts
cp robot_events/*.py ../docker/sic/robot_scripts
cp robot_microphone/*.py ../docker/sic/robot_scripts
cp robot_puppet/*.py ../docker/sic/robot_scripts
cp -r robot_scripts/* ../docker/sic/robot_scripts
cd ../output
cp robot_actions/*.py ../docker/sic/robot_scripts
cp robot_sound/*.py ../docker/sic/robot_scripts
cp robot_tablet/*.py ../docker/sic/robot_scripts

cd ../processing
echo Done!
