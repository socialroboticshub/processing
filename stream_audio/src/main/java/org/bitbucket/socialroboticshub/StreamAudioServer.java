package org.bitbucket.socialroboticshub;

public final class StreamAudioServer {
	private static final int streamAudioPort = 11883;
	private final StreamAudio streamAudio;

	public static void main(final String... args) {
		try {
			final StreamAudioServer audioserver = new StreamAudioServer();
			audioserver.run();
		} catch (final Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}

	private StreamAudioServer() throws Exception {
		this.streamAudio = new StreamAudio(streamAudioPort);
	}

	public void run() throws Exception {
		System.out.println("Starting audio-socketserver on " + streamAudioPort + "...");
		this.streamAudio.run();
	}
}
