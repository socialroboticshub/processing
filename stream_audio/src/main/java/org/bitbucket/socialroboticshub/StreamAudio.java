package org.bitbucket.socialroboticshub;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.Protocol;

final class StreamAudio extends WebSocketServer {
	private final Map<String, WebSocket> connections;
	private final Map<String, SocketListener> listeners;

	StreamAudio(final int port) throws Exception {
		super(new InetSocketAddress(port));
		this.connections = new ConcurrentHashMap<>();
		this.listeners = new ConcurrentHashMap<>();
	}

	@Override
	public void onOpen(final WebSocket conn, final ClientHandshake handshake) {
	}

	@Override
	public void onClose(final WebSocket conn, final int code, final String reason, final boolean remote) {
		final Iterator<Entry<String, WebSocket>> iterator = this.connections.entrySet().iterator();
		while (iterator.hasNext()) {
			final Entry<String, WebSocket> connection = iterator.next();
			if (connection.getValue() == conn) { // direct == should work here
				final String identifier = connection.getKey();
				System.out.println("Closing audio-socket with '" + identifier + "'...");
				this.connections.remove(identifier);
				this.listeners.get(identifier).disconnect();
				break;
			}
		}
	}

	@Override
	public void onMessage(final WebSocket conn, final String message) {
		final WebSocket existing = this.connections.get(message);
		if (existing != null) {
			try {
				existing.close(1000, "reconnect");
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		this.connections.put(message, conn);
		if (!this.listeners.containsKey(message)) {
			try {
				final SocketListener listener = new SocketListener(message);
				listener.start();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onMessage(final WebSocket conn, final ByteBuffer message) {
		onMessage(conn, StandardCharsets.UTF_8.decode(message).toString());
	}

	@Override
	public void onError(final WebSocket conn, final Exception ex) {
		ex.printStackTrace();
	}

	@Override
	public void onStart() {
	}

	private final class SocketListener extends Thread {
		private final String identifier;
		private final Jedis publisher, subscriber;
		private final JedisPubSub pubsub;
		private RedisAudioIterator audio;

		SocketListener(final String identifier) throws Exception {
			this.identifier = identifier;
			this.publisher = connect();
			this.subscriber = connect();
			this.pubsub = new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					switch (message) {
					case "ListeningStarted":
						if (SocketListener.this.audio == null) {
							try {
								SocketListener.this.audio = new RedisAudioIterator(SocketListener.this.identifier);
								new Thread(() -> {
									while (SocketListener.this.audio != null && SocketListener.this.audio.hasNext()) {
										final byte[] next = SocketListener.this.audio.next();
										if (next != null) {
											send(next);
										}
									}
								}).start();
							} catch (final Exception e) {
								e.printStackTrace();
							}
						} else {
							System.err.println(SocketListener.this.identifier + " already streaming audio...");
						}
						break;
					case "ListeningDone":
						if (SocketListener.this.audio != null) {
							SocketListener.this.audio.close();
							SocketListener.this.audio = null;
						} else {
							System.err.println(SocketListener.this.identifier + " already not streaming audio...");
						}
						break;
					}
				}
			};
			StreamAudio.this.listeners.put(identifier, this);
		}

		public void disconnect() {
			this.pubsub.unsubscribe();
			this.subscriber.disconnect();
			this.publisher.disconnect();
			StreamAudio.this.listeners.remove(this.identifier);
		}

		private void send(final byte[] payload) {
			final WebSocket socket = StreamAudio.this.connections.get(this.identifier);
			if (socket != null) {
				try {
					socket.send(payload);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void run() {
			System.out.println("Subscribing '" + this.identifier + "'");
			this.subscriber.subscribe(this.pubsub, this.identifier + "_events");
		}
	}

	private final class RedisAudioIterator implements Iterator<byte[]>, Closeable {
		private final Jedis redis;
		private final byte[] audiostreamtopic;
		private final List<byte[]> allbytes;
		private int totalSize = 0;
		private volatile boolean closed = false;

		RedisAudioIterator(final String identifier) throws Exception {
			this.redis = connect();
			this.audiostreamtopic = (identifier + "_audio_stream").getBytes(StandardCharsets.UTF_8);
			this.allbytes = new ArrayList<>();
		}

		@Override
		public boolean hasNext() {
			if (this.closed) {
				this.redis.close();
				return false;
			} else {
				return true;
			}
		}

		@Override
		public byte[] next() {
			byte[] next;
			do {
				next = this.redis.lpop(this.audiostreamtopic);
				if (next == null) { // nothing yet; wait
					try {
						Thread.sleep(1);
					} catch (final InterruptedException ignore) {
					}
				}
			} while (next == null && !this.closed);
			if (next != null) {
				this.allbytes.add(next);
				this.totalSize += next.length;
			}
			return next;
		}

		@Override
		public void close() {
			this.closed = true;
			try {
				final ByteBuffer data = ByteBuffer.allocate(this.totalSize);
				for (final byte[] chunk : this.allbytes) {
					normalizeVolume(chunk);
					data.put(chunk);
				}
				final byte[] wav = PCMtoWav(data.order(ByteOrder.LITTLE_ENDIAN).rewind(), 16000);
				final String fileName = dateFormat.format(new Date()) + ".wav";
				final FileOutputStream out = new FileOutputStream(new File(fileOutputPath, fileName));
				out.write(wav);
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static byte[] PCMtoWav(final ByteBuffer data, final int sampleRate) throws Exception {
		final ByteArrayOutputStream output = new ByteArrayOutputStream();
		writeString(output, "RIFF"); // chunk id
		writeInt(output, 36 + data.capacity()); // chunk size
		writeString(output, "WAVE"); // format
		writeString(output, "fmt "); // subchunk 1 id
		writeInt(output, 16); // subchunk 1 size
		writeShort(output, (short) 1); // audio format (1 = PCM)
		writeShort(output, (short) 1); // number of channels
		writeInt(output, sampleRate); // sample rate
		writeInt(output, sampleRate * 2); // byte rate (samplerate*channels*bits/8)
		writeShort(output, (short) 2); // block align (channels*bits/8)
		writeShort(output, (short) 16); // bits per sample
		writeString(output, "data"); // subchunk 2 id
		writeInt(output, data.capacity()); // subchunk 2 size
		final short[] shorts = new short[data.capacity() / 2];
		data.asShortBuffer().get(shorts);
		for (final short s : shorts) {
			writeShort(output, s);
		}
		return output.toByteArray();
	}

	private static void writeInt(final OutputStream output, final int value) throws Exception {
		output.write(value >> 0);
		output.write(value >> 8);
		output.write(value >> 16);
		output.write(value >> 24);
	}

	private static void writeShort(final OutputStream output, final short value) throws Exception {
		output.write(value >> 0);
		output.write(value >> 8);
	}

	private static void writeString(final OutputStream output, final String value) throws Exception {
		for (int i = 0; i < value.length(); i++) {
			output.write(value.charAt(i));
		}
	}

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	private static final File fileOutputPath = Paths.get("/data").toFile();
	private static int N_SHORTS = 0xffff;
	private static final short[] VOLUME_NORM_LUT = new short[N_SHORTS];
	private static int MAX_NEGATIVE_AMPLITUDE = 0x8000;
	static {
		for (int s = 0; s < N_SHORTS; s++) {
			final double v = (s - MAX_NEGATIVE_AMPLITUDE);
			final double sign = Math.signum(v);
			VOLUME_NORM_LUT[s] = (short) sign;
			VOLUME_NORM_LUT[s] *= (1.240769e-22 - (-4.66022 / 0.0001408133) * (1 - Math.exp(-0.0001408133 * v * sign)));
		}
	}

	private static void normalizeVolume(final byte[] audioSamples) {
		for (int i = 0; i < audioSamples.length; i += 2) {
			short s1 = audioSamples[i + 1];
			short s2 = audioSamples[i];
			s1 = (short) ((s1 & 0xff) << 8);
			s2 = (short) (s2 & 0xff);
			short res = (short) (s1 | s2);
			res = VOLUME_NORM_LUT[Math.min(res + MAX_NEGATIVE_AMPLITUDE, N_SHORTS - 1)];
			audioSamples[i] = (byte) res;
			audioSamples[i + 1] = (byte) (res >> 8);
		}
	}

	private Jedis connect() throws Exception {
		Jedis jedis;
		final boolean selfsigned = "1".equals(System.getenv("DB_SSL_SELFSIGNED"));
		if (selfsigned) {
			final KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(StreamAudio.class.getResourceAsStream("/truststore.jks"), "changeit".toCharArray());
			final Certificate original = ((KeyStore.TrustedCertificateEntry) keyStore.getEntry("sic", null))
					.getTrustedCertificate();
			final TrustManager bypass = new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[] { (X509Certificate) original };
				}

				@Override
				public void checkClientTrusted(final X509Certificate[] chain, final String authType)
						throws CertificateException {
					checkServerTrusted(chain, authType);
				}

				@Override
				public void checkServerTrusted(final X509Certificate[] chain, final String authType)
						throws CertificateException {
					if (chain.length != 1 || !chain[0].equals(original)) {
						throw new CertificateException("Invalid certificate provided");
					}
				}
			};
			final SSLContext sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, new TrustManager[] { bypass }, null);
			final SSLSocketFactory sslFactory = sslContext.getSocketFactory();
			jedis = new Jedis(System.getenv("DB_IP"), Protocol.DEFAULT_PORT, true, sslFactory, null, null);
		} else {
			jedis = new Jedis(System.getenv("DB_IP"), Protocol.DEFAULT_PORT, true);
		}
		jedis.auth(System.getenv("DB_PASS"));
		return jedis;
	}
}
